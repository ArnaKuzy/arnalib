
/* alert_box */
function alert_box(color, text) {
	var alert_box = '#alert_box';
	
    $(alert_box).remove();
    $('body').append('<div id="alert_box"></div>');
    $(alert_box).text(text);
    $(alert_box).css('margin-left',
	   '-' + $(alert_box).width() / 2 + 'px');
	$(alert_box).addClass(color);
    $(alert_box).fadeIn().delay(1500).fadeOut(2000);
}