<?php

require '../Arnalib/Autoload.php';
Arnalib\Autoload::register();

use Arnalib\HTML\Tag;

// Init App
$app = new Arnalib\CORE\App();
$page = Arnalib\CORE\Page::getInstance();
$page->setDir(__DIR__);
$params = $page->getParams();
if ($page->process)
	include $page->process;

if ($page->template)
{
	// Menu
	ob_start();
	echo Tag::gen_ul_menu(array(
		[ '/',		'Accueil' ]
	));
	$menu = ob_get_clean();
	
	// page content loading
	ob_start();
	include $page->body;
	$content = ob_get_clean();
	include "../pages/templates/default.php";
}