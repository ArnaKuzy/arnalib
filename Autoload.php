<?php

namespace Arnalib;

class Autoload
{
	/**
	 * Create a register of class for autoloader
	 * Call autoloader of this class
	 * 
	 * @return void
	 */
	public static function register()
	{
		spl_autoload_register( array( __CLASS__, 'autoload' ) );
	}

	/**
	 * Include the file associated to us class
	 * 
	 * @param [type] $class string The name of the class to load
	 * @return void
	 */
	private static function autoload($class)
	{
		// MANDATORY FOR linux replace \ by /
		$class = str_replace('\\', '/', $class);

		// get root path
		$dir = str_replace('\\', '/', dirname(__DIR__));

		// For framework class
		if (file_exists("$dir/$class.php"))
			require "$dir/$class.php";
		// for externe class
		else if (file_exists("$dir/src/$class.php"))
			require "$dir/src/$class.php";
	}
}