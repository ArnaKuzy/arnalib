<?php

namespace Arnalib\Menu;

use \Arnalib\HTML\Form;
use \Arnalib\HTML\Tag;

class Item
{
	use \Arnalib\CORE\Get;
	use \Arnalib\CORE\Set;
	use \Arnalib\CORE\Hydrate;

	protected $_id;
	protected $_id_type = null;
	protected $_id_picture = null;
	protected $_picture = null;
	protected $_name = null;
	protected $_price = '0';

	public function __construct(array $data = [])
	{
		if (!empty($data))
			self::hydrate($data);
	}

	public function req_values()
	{
		$values = [];

		foreach (get_object_vars($this) as $key => $value) {
			if (!is_null($value)) {
				$key = substr($key, 1);
				$values[$key] = $value;
			}
		}
		unset($values['id']);

		//var_dump($values);

		return $values;
	}

	public function setId_type($value)
	{
		if ($value > 0)
			$this->_id_type = $value;
	}

	public function setName(string $value)
	{
		if (!is_null($value) && !empty($value) && $value != '')
			$this->_name = \ucfirst(\strtolower($value));
	}

	public function setPrice($value)
	{
		if (!is_null($value) && !empty($value))
			$this->_price = $value;
	}

	/*
	public function setId_picture(?int $value)
	{
		if ($value > 0)
			$this->_id_picture = $value;
	}
	*/

	public function displa(string $var): string
	{
		if ($var == "id_picture")
			if ($this->$var == 0)
				return '0.png';
		return htmlspecialchars($this->$var);
	}

	public function getPicture()
	{
		if (is_null($this->_picture))
			return new \Arnalib\Picture\Picture([ 'name' => 'default', 'file_key' => '0.jpg' ]);
		
		return $this->_picture;
	}

	public function form(string $item_name, array $picture_list)
	{
		$data = array();
		$data['name'] = $this->name;
		$data['price'] = $this->price;
		$data['id_picture'] = $this->_id_picture;

		$form = new Form($data);

		var_dump($data);

		$html =
			$form->input('id', 'hidden', [ 'value' => $this->id ] ) .
			$form->input('type', 'hidden', [ 'value' => $item_name ] ) .
			$form->linear_surround('name', 'Nom du plat', $form->input('name', 'text', [ 'class' => 'input' ])) .
			'Image : ' . Tag::tag('div', $form->select('id_picture', $picture_list ), [ 'class' => 'select' ]) .
			$form->linear_surround('price', 'Prix', $form->input('price', 'number', [ 'class' => 'input' ]));
		
		return $html;
	}
}