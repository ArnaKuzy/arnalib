<?php

namespace Arnalib\Menu;

use \Arnalib\HTML\Form;
use \Arnalib\HTML\Tag;

class Dish extends Item
{
	use \Arnalib\CORE\Get;
	use \Arnalib\CORE\Set;
	use \Arnalib\CORE\Hydrate;

	private $_ingredients = array();
	
	public function __construct(array $data = [])
	{
		if (!empty($data)) {
			$needle = 'ingredient_';
			$i = 0;
			
			foreach ($data as $key => $value) {
				$pos = strpos($key, $needle);
				if (is_int($pos)) {
					$this->_ingredients[] = substr($key, strlen($needle));
					unset($data[$i]);
				}
				++$i;
			}
			
			self::hydrate($data);
		}
	}

	public function display(int $index)
	{
		$ingredients = '';

		foreach($this->_ingredients as $ingredient)
			$ingredients .= "<span><img src=\"http://optitcreux.admin/img/upload/{$ingredient->picture->file_key}\" class=\"ingredient-picture\"> $ingredient->name</span>";

		$img = "<img src=\"http://optitcreux.admin/img/upload/{$this->picture->file_key}\" class=\"dish-img\">";
		$content = "<p>
				<span>
					<strong>$this->name</strong>
				</span>
				<br><br>
				<span class=\"ingredient-parent\">$ingredients</span>
			</p>";
		$price = "<p class=\"price\">$this->price €</p>";

		$display = ($index % 2 == 0) ?
			$img . $content . $price :
			$price . $content . $img;

		$html = Tag::tag('div', $display , [ 'class' => 'dish' ]);

		return $html;
	}

	public function d_form($ingredient_list, $picture_list)
	{
		foreach ($this->_ingredients as $ingredient)
			$data["ingredient_$ingredient->id"] = true;
		$data['name'] = $this->name;
		$data['price'] = $this->price;
		$data['id_picture'] = $this->_id_picture;

		$form = new Form($data);

		//var_dump($data);

		$ingredients = '';
		foreach ($ingredient_list as $type) {
			$ingredients .= "<hr class=\"has-background-grey-lighter\"><span class=\"subtitle\">{$type['name']}</span>";
			foreach ($type['ingredients'] as $item)
				$ingredients .= ' ' . Tag::tag('label', $form->checkbox("ingredient_$item->id", [ 'class' => 'checkbox' ]) . " <strong>$item->name</strong>");
		}

		$html =
			$form->input('id', 'hidden', [ 'value' => $this->id ] ) .
			$form->input('type', 'hidden', [ 'value' => 'dish' ] ) .
			$form->linear_surround('name', 'Nom du plat', $form->input('name', 'text', [ 'class' => 'input' ])) .
			'Image : ' . Tag::tag('div', $form->select('id_picture', $picture_list ), [ 'class' => 'select' ]) .
			$form->linear_surround('price', 'Prix', $form->input('price', 'number', [ 'class' => 'input' ])) .
			Tag::tag('p', $ingredients);
		
		return $html;
	}
}