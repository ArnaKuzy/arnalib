<?php

namespace Arnalib\Menu;

use Arnalib\CORE\Db;
use Arnalib\Menu;

class MenuManager
{
	use \Arnalib\CORE\GetInstance;
	private static $_instance;

	const	ITEM_CLASS = '\Arnalib\Menu\Item';
	const	DISH_CLASS = '\Arnalib\Menu\Dish';
	const	MENU_CLASS = '\Arnalib\Menu\Menu';
	const	INGREDIENT_CLASS = '\Arnalib\Menu\Ingredient';

	private $_db;

	public function	__construct()
	{
		$this->_db = Db::getInstance();
	}

	public function create_tables()
	{
		$this->_db->create_table('dish.sql');
		$this->_db->create_table('dish_type.sql');
		$this->_db->create_table('dish_ingredients.sql');
		$this->_db->create_table('ingredient.sql');
		$this->_db->create_table('ingredient_type.sql');
	}

	/**
	 * ADD
	 */

	public function add_item(string $item_name, array $data): string
	{
		$item = ($item_name == 'dish') ? new Dish($data) : new Ingredient($data);

		if (is_null($item->name) || is_null($item->id_type))
			return 'add_fail "invalid data"';

		if (!$this->_db->insert($item_name, $item->req_values()))
			return 'add_fail "db fail';

		if ($item_name == 'dish') {
			$item->id = $this->_db->last_insert_id();
			foreach ($item->ingredients as $id) {
				$this->add_dish_ingredient($item->id, $id);
			}
		}
		return 'add_success';
	}

	public function add_type(string $item, array $data)
	{
		if (!isset($data['name']))
			return 'add_fail "missing data"';

		$name = $data['name'];

		if (strlen($name) > 20)
			return 'add_fail "too long"';
		if (!preg_match('/^[a-zA-Z0-9 ]+/', $name))
			return 'add_fail "bad_character"';
		if (!$this->_db->insert($item , [ 'name' => $name ]))
			return 'add_fail "db fail';
		return 'add_success';
	}

	public function add_dish_ingredient($dish_id, $ingredient_id)
	{
		if ($this->_db->insert('dish_ingredients', [ 'id_dish' => $dish_id, 'id_ingredient' => $ingredient_id ]))
			return true;
		return false;
	}
	
	/**
	 *  ^ ADD
	 * 
	 * GET
	 */

	public function list(string $item): ?array
	{
		$req = $this->_db->select('*', $item);

		if ($req) {
			$req->setFetchMode(\PDO::FETCH_CLASS, self::ITEM_CLASS);
			return $req->fetchAll();
		}
		return null;
	}

	public function get(string $item, $id)
	{
		$result = null;
		$class = "\Arnalib\Menu\\" . ucfirst($item);

		$req = $this->_db->select('*', $item, 'WHERE id = ?', [ $id ]);

		if ($req) {
			$req->setFetchMode(\PDO::FETCH_CLASS, $class);
			$result = $req->fetch();

			//var_dump($result);

			if ($result && $item == 'dish') {
				$result->ingredients = $this->get_ingredient_by_dish_id($result->id);
				//var_dump($result);
			}
		}
		return $result;
	}

	public function get_types($item): ?array
	{
		$req = $this->_db->select('*', $item.'_type');

		if ($req) {
			$types = [];
			$data = $req->fetchAll(\PDO::FETCH_ASSOC);
			foreach ($data as $type)
				$types[$type['name']] = $type['id'];
			return $types;
		}
		return null;
	}

	public function get_item_list_by_type(string $item_name, int $id_type)
	{
		$req = $this->_db->select('*', $item_name, 'WHERE id_type = ?', [ $id_type ]);

		if ($req) {
			$item = ($item_name == 'dish') ? self::DISH_CLASS : self::INGREDIENT_CLASS;
			$req->setFetchMode(\PDO::FETCH_CLASS, $item);
			$data = $req->fetchAll();

			foreach ($data as $item) {
				$item->picture = \Arnalib\Picture\PictureManager::getInstance()->get($item->id_picture);
				if ($item_name == 'dish') {
					//var_dump($item);
					$item->ingredients = $this->get_ingredient_by_dish_id($item->id);
				}
			}

			return $data;
		}
		return null;
	}

	public function get_ingredient_by_dish_id(int $dish_id)
	{
		$req = $this->_db->select('I.*', 'ingredient AS I INNER JOIN dish_ingredients as DI ON I.id = DI.id_ingredient', 'WHERE DI.id_dish = ?', [ $dish_id ]);
//		var_dump($req, $dish_id);

		if ($req) {
			$req->setFetchMode(\PDO::FETCH_CLASS, self::INGREDIENT_CLASS);
			return $req->fetchAll();
		}
		return null;
	}

	public function get_menu_list()
	{
		$menu_list = array();

		foreach ($this->get_types('dish') as $dish_type_name => $dish_type_id) {
			$menu = new \Arnalib\Menu\Menu();

			$menu->name = $dish_type_name;
			$menu->dishes = $this->get_item_list_by_type('dish', $dish_type_id);

			foreach ($menu->dishes as $key => $dish) {
				$dish->ingredients = $this->get_ingredient_by_dish_id($dish->id);
			}

			$menu_list[] = $menu;
		}
		return $menu_list;
	}

	/**
	 * ^ GET
	 * 
	 * UPDATE
	 */
	public function update_item(string $item_name, array $data)
	{
		$item = ($item_name == 'dish') ? new Dish($data) : new Ingredient($data);

		//return var_dump($data);

		if (is_null($item->name))
			return json_encode( [ 'success' => false, 'error' => 'add_fail invalid data' ] );

		if ($item_name == 'dish') {
			$dish_ingredients = $this->get_ingredient_by_dish_id($item->id);

			$needle = 'ingredient_';
			$ingredient_ids = array();

			foreach ($data as $key => $value) {
				if (strstr($key, $needle))
					$ingredient_ids[] = substr($key, strlen($needle));
			}

			foreach($dish_ingredients as $ingredient)
				if (!in_array($ingredient->id, $ingredient_ids)) {
					var_dump($this->remove_ingredient_from_dish($item->id, $ingredient->id));
				} else
					unset($ingredient_ids[array_search($ingredient->id, $ingredient_ids)]);
			
			foreach($ingredient_ids as $key => $value)
				$this->add_dish_ingredient($item->id, $value);

			//var_dump($dish_ingredients, $data, $ingredient_ids);
		}

		//return var_dump($item);

		if (!$this->_db->update(
			$item_name,
			[ 'name' => '', 'id_picture' => '', 'price' => '' ],
			[
				'id'			=> $item->id,
				'name'			=> $item->name,
				'id_picture'	=> $item->id_picture,
				'price'			=> $item->price
			],
			'WHERE id = :id'))
			return [ 'success' => false, 'msg' => 'add_fail "db fail' ];
		//header('location:/menu');
		return [ "success" => true ];
	}

	/**
	 * ^ UPDATE
	 * 
	 * DELETE
	 */

	public function remove_ingredient_from_dish(int $dish_id, int $ingredient_id)
	{
		if (!$this->_db->delete('dish_ingredients WHERE id_dish = ? AND id_ingredient = ?', [ $dish_id, $ingredient_id ]))
			return false;
		return true;
	}

	public function delete_item(string $item, $id)
	{
		if ($this->_db->delete($item . ' WHERE id = ?', [ $id ]))
			return 'delete_success';
		return 'delete_fail';
	}

	public function delete_type(string $type, $id)
	{
		$req = $this->_db->select('id', substr($type, 0, strpos($type, '_')), 'WHERE id_type = ?', [ $id ]);

		if (!empty($req->fetch()))
			return 'delete fail "Type always used"';
		if ($this->_db->delete($type . ' WHERE id = ?', [ $id ]))
			return 'delete_success';
		return 'delete_fail';
	}
}