<?php

namespace Arnalib\Menu;

class Menu
{
	use \Arnalib\CORE\Get;
	use \Arnalib\CORE\Set;
	use \Arnalib\CORE\Hydrate;

    private $_name;
	private $_dishes = array();

	public function setName(string $value)
	{
		if (!is_null($value) && !empty($value) && $value != '')
			$this->_name = \ucfirst(\strtolower($value));
	}
}