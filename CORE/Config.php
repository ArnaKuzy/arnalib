<?php

namespace Arnalib\CORE;

class Config
{
	/**
	 * Singleton
	 */
	use GetInstance;
	private static $_instance;
	
	/**
	 * Website settings (database, etc)
	 *
	 * @var array
	 */
	private $settings = null;

	/**
	 * Load conf file
	 */
	public function __construct()
	{
		$this->settings = require dirname(__DIR__, 2) . '/config/config.php';
	}

	public function getSettings()
	{
		return $this->settings;
	}

	public function get($key)
	{
		return $this->settings[$key];
	}
}