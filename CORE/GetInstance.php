<?php

namespace Arnalib\CORE;

trait GetInstance
{
	/**
	 * Get the instance
	 * If the instance is null, create a new instance
	 * Else return self instance
	 *
	 * @return __CLASS__|null
	 */
	public static function getInstance()
	{
		$class = __CLASS__;

		if (is_null(self::$_instance)) {
			self::$_instance = new $class();
		}
		return self::$_instance;
	}
}