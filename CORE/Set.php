<?php

namespace Arnalib\CORE;

trait Set
{
	/**
	 * Call set function for private var
	 *
	 * @param string $key
	 * @return void
	 */
	public function __set(string $key, $value)
	{
        $method = 'set' . ucfirst($key);
        $key = "_$key";
        if (method_exists($this, $method))
            $this->$method($value);
        else
            $this->$key = $value;
	}
}