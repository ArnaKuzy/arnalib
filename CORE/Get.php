<?php

namespace Arnalib\CORE;

trait Get
{
	/**
	 * Call get function for private var
	 *
	 * @param string $key
	 * @return void
	 */
	public function __get(string $key)
	{
		$method = 'get' . ucfirst($key);
		$key = "_$key";
		if (method_exists($this, $method))
			$val = $this->$method();
		else
			$val = $this->$key;
		return $val;
	}
}