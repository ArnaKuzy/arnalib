<?php

namespace Arnalib\CORE;

class Db
{
	/**
	 * Singleton
	 */
	use GetInstance;
	private static $_instance;

	/**
	 * Variables
	 */
	private $db;
	private $host = null;
	private $name = null;
	private $user = null;
	private $pass = null;

	public function __construct()
	{
		// Get bdd config
		$settings = Config::getInstance()->getSettings();

		// Hydrate
		if (array_key_exists('db_host', $settings) &&
			array_key_exists('db_name', $settings) &&
			array_key_exists('db_user', $settings) &&
			array_key_exists('db_pass', $settings))
		{
			$this->host = $settings['db_host'];
			$this->name = $settings['db_name'];
			$this->user = $settings['db_user'];
			$this->pass = $settings['db_pass'];
		}
	}
	
	public function connect()
	{
		// if $db is null
		if (is_null($this->db)) {
			// Try to connect db
			try
			{
				$this->db = new \PDO("mysql:host={$this->host};dbname={$this->name}", $this->user, $this->pass);
			}
			catch (\Exception $e)
			{
				// If db unavailable contact webmaster and block website
				Error::dbFatalError($e);
				die('503 : Service indisponible.');
			}
		}
	}

	public function crud(string $sql, array $values = null)
	{
		if (is_null($this->db))
			$this->connect();
		if ($this->db) {
			if (is_null($values))
				$req = $this->db->query($sql);
			else {
				$req = $this->db->prepare($sql);
				$req->execute($values);
			}
			return $req;
		} else
			return null;
	}

	/**
	 * Fill table(*) && VALUES(*) of insert into request
	 *
	 * @param array $values // values to insert
	 * @param integer $type // if is for table() or VALUES()
	 * @return string // Return line
	 */
	private function fill_values(array $values, int $type): string
	{
		// 0 = table() or 1 = VALUES()
		$content = ($type == 0) ? '' : ':';

		foreach ($values as $key => $v) {
			if (strlen($content) > 1)
				$content .= ($type == 0) ? ', ' : ', :';
			$content .= $key;
		}
		return $content;
	}

	/**
	 * Insert Into SQL method
	 *
	 * @param string $table // table to insert
	 * @param array $values // cols to insert
	 * @return void // return result of request
	 */
	public function insert(string $table, array $values)
	{
		if (empty($values))
			return null;
		return $this->crud("INSERT INTO $table(". $this->fill_values($values, 0) .") VALUES(". $this->fill_values($values, 1) .")", $values);
	}

	public function multi_insert(string $table, array $values)
	{
		if (empty($values))
			return null;
			
		if (is_null($this->db))
			$this->connect();
		
		if ($this->db) {
			try {

				$req = $this->db->prepare("INSERT INTO $table(". $this->fill_values($values[0], 0) .") VALUES(". $this->fill_values($values[0], 1) .")");
				foreach ($values as $value) {
					$req->execute($value);
				}
			}
			catch (PDOException $e) {
                die($e->getMessage());
			}
		} else
			return null;
	}

	/**
	 * Select SQL method
	 *
	 * @param string $cols // Cols to select
	 * @param string $table // Table to select
	 * @param string $options // req options (WHERE, ORDER BY, etc)
	 * @param array $values // values for options
	 * @return void // return result of request
	 */
	public function select(string $cols, string $table, string $options = '', array $values = null)
	{
		return $this->crud("SELECT $cols FROM $table $options", $values);
	}

	private function fill_update(array $values)
	{
		// 0 = table() or 1 = VALUES()
		$content = '';

		foreach ($values as $key => $v) {
			if (strlen($content) > 1)
				$content .= ', ';
			$content .= "$key = :$key";
		}
		return $content;
	}

	/**
	 * Update SQL method
	 *
	 * @param string $table // Table to update
	 * @param array $values // Values to update
	 * @param string $options // Options (WHERE, etc)
	 * @return void // return result of request
	 */
	public function update(string $table, array $sets, array $values, string $options = '')
	{
		return $this->crud("UPDATE $table SET ". $this->fill_update($sets) . " $options", $values);
	}

	/**
	 * Delete SQL method
	 *
	 * @param string $sql // req sql table + options (WHERE, etc)
	 * @param array $values
	 * @return void // Result of request
	 */
	public function delete(string $sql, array $values = null)
	{
		return $this->crud("DELETE FROM $sql", $values);
	}

	/**
	 * Create table
	 */
	public function create_table(string $table)
	{
		$req = file_get_contents(dirname(__DIR__) . "/tables/$table");
		$this->crud($req);
	}

	public function last_insert_id()
	{
		return $this->db->lastInsertId();
	}
}