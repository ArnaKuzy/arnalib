<?php

namespace Arnalib\CORE;

class App
{
	/**
	 * Singleton
	 */
	use GetInstance;
	private static $_instance;

	public function __construct()
	{
		$this->run();
	}

	/**
	 * Start app, redirect to correct uri, call Page controller
	 *
	 * @return void
	 */
	public function run()
	{
		$uri = $_SERVER['REQUEST_URI'];
		// Redirect to link without '/' at the end
		if (!empty($uri) && $uri[-1] === '/' && $uri != '/') {
			header('Location: ' . substr($uri, 0, -1));
			header('HTTP/1.1 301 Moved Permanentely');
		}

		// Instanciate Page class and run uri control
		Page::getInstance()->run($uri);
	}
}