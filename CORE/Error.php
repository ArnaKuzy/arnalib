<?php

namespace Arnalib\CORE;

class Error
{
	public static function contactWebmaster($who, $error)
	{
		$to      = Config::getInstance()->get('webmaster_email');
		$subject = 'PANNE SERVEUR';
		$message = 	"PANNE SERVEUR " . $_SERVER['SERVER_NAME'] .
					" \r\n \r\n $who \r\n \r\n".
					$error;
		$headers = array(
			'From' => 'webmaster@arnastudio.fr',
			'X-Mailer' => 'PHP/' . phpversion()
			);

		mail($to, $subject, $message, $headers);
	}

	public static function dbFatalError($error)
	{
		self::contactWebmaster('Db connect', $error);
		die("Cette pas n'est pas disponible");
	}
}