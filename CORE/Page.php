<?php

namespace Arnalib\CORE;

class Page
{
	use Get;
	use GetInstance;
	private static $_instance;
	private $_params = array();
	private $_home = 'home';
	private $_current_page;
	private $_template = true;
	private $dir;

	public function run(string $url, string $sep = '/')
	{
		$this->_params = $this->parse($url, $sep);

		if (is_null($this->_params[0]))
			$this->_current_page = $this->_home;
		else {
			if ($this->_params[0] == 'j') {
				$this->_template = false;
				array_shift($this->_params);
			}
			$this->_current_page = $this->_params[0];
		}
	}

	/**
	 * Parse an 'URL' width separator 'sep'
	 * 'sep' = '/' by default
	 *
	 * Return an array of parsed string
	 * Or null if no string parsed
	 *
	 * @param string $url
	 * @return array|null
	 */
	private function parse(string $url, string $sep = '/'): ?array
	{
		$values = array();

		// Check patern => /arg/arg/arg <=
		$regex = '#' . '^(?:/|(?:/[\w-]+)+)$' . '#';
		if (preg_match($regex, $url) == 0)
			return array('404');

		// Remove first /
		$url = substr($url, 1);

		// Extract arguments and subtract it from url
		while ($pos = strpos($url, $sep)) {
			$values[] = substr($url, 0, $pos);
			$url = substr($url, ++$pos);
		}

		// Get last element from purged url
		if (!empty($url))
			$values[] = $url;

		// if args are empty return null
		if (empty($values))
			return null;
		return $values;
	}

	/**
	 * SETTERS
	 */

	/**
	 * Set defaultPage name to $name
	 *
	 * @param string $name
	 * @return void
	 */
	public function setCurrentPage(string $name)
	{
		if (!is_null($name))
			$this->_current_page = $name;
	}

	public function setDir($dir)
	{
		$this->dir = $dir;
	}

	/**
	 * GETTERS
	 */

	/**
	 * Return name of current page
	 *
	 * @return string
	 */
	public function getPage(): string { return $this->_current_page; }

	/**
	 * Return url parameters
	 *
	 * @return array|null
	 */
	public function getParams(): ?array { return $this->_params; }

	/**
	 * include processing file of the page if exists
	 *
	 * @return void
	 */
	public function getProcess(): ?string
	{
		$process = dirname($this->dir) . '/process/' . $this->_current_page . '.php';
		if (file_exists($process))
			return $process;
		return null;
	}

	/**
	 * If exists return link css of current page
	 *
	 * @return string|null
	 */
	public function getCss(): ?string
	{
		$css = $this->dir . '/css/' . $this->_current_page . '.css';
		if (file_exists($css))
			return '<link rel="stylesheet" href="/css/'. $this->_current_page .'.css">';
		return '<link rel="stylesheet" href="/css/404.css">';
	}

	/**
	 * If exist return include dir of file
	 * else return 404 include dir
	 *
	 * @return string
	 */
	public function getBody(): string
	{
		$page = dirname($this->dir) . '/pages/' . $this->_current_page . '.php';
		if (file_exists($page))
			return $page;
		else
			return dirname($this->dir) . '/pages/404.php';
	}

	/**
	 * If exists return src js of current page
	 *
	 * @return string|null
	 */
	public function getJs(): ?string
	{
		$js = $this->dir . '/js/' . $this->_current_page . '.js';
		if (file_exists($js))
			return '<script src="/js/' . $this->_current_page . '.js"></script>';
		return null;
	}
}