<?php

namespace Arnalib\CORE;

trait Hydrate
{
	// Hydratation
	protected function hydrate(array $data)
	{
		foreach ($data as $key => $value)
		{
			if (property_exists(__CLASS__, "_$key"))
				$this->$key = $value;
		}
	}
}