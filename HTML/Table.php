<?php

namespace Arnalib\HTML;

use Arnalib\HTML\Tag;

class Table
{
	private static function fill($content, string $tag, array $attr = [])
	{
		$result = '';

		switch ($tag) {
			case 'table':
				foreach ($content as $value)
					$result .= self::fill($value, 'tr');
				break;
					
			case 'tr':
				foreach ($content as $value)
					$result .= self::fill($value, 'td');
				break;
			
			case 'td':
			case 'th':
				$result .= $content;
				break;

			default:
				return null;
		}

		return Tag::tag($tag, $result, $attr);
	}

	public static function makeTable(array $content, array $attr = [])
	{
		return self::fill($content, 'table');
	}

	public static function row(array $content, array $attr = [])
	{
		return self::fill($content, 'tr');
	}

	public static function col(string $content, array $attr = [])
	{
		return self::fill($content, 'td');
	}

	public static function header(array $content, array $attr = [])
	{
		$result = '';

		foreach ($content as $value)
			$result .= self::fill($value, 'th');
		
		$result = Tag::tag('tr', $result, $attr);
		return $result;
	}
}