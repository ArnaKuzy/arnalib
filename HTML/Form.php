<?php

namespace Arnalib\HTML;

use Arnalib\HTML\Tag;

class Form
{
	private $_data;

	public function __construct($data = null)
	{
		$this->_data = $data;
	}

	/**
	 * Return input element
	 *
	 * @param string $name name of input (pseudo, email, ...)
	 * @param string $type type of input (text, password, hidden, ...)
	 * @param string $value default value
	 * @param array $attr list of attributes tag attr (id, class, ...)
	 * @return string return formated input
	 */
	public function input(string $name, string $type = 'text', $attr = null): string
	{
		if ($type != 'checkbox' && isset($this->_data[$name]))
			$attr['value'] = $this->_data[$name];

		return '<input type="'. $type .'" name="'. $name .'" '. Tag::attr($attr) .'>';
	}

	/**
	 * Return textarea
	 *
	 * @param string $name
	 * @param string $value
	 * @param integer $cols
	 * @param integer $rows
	 * @param string $id
	 * @param string $class
	 * @return void
	 */
	public function textarea(string $name, string $value = null, int $cols = 30, int $rows = 10, array $attr = [])
	{
		if (isset($this->_data[$name]))
			$value = $this->_data[$name];

		$attr = array_merge(
			$attr,
			array(
				'name'	=> $name,
				'cols' 	=> $cols,
				'rows'	=> $rows
			)
		);
		return Tag::tag('textarea', $value, $attr);
	}

	/**
	 * Return formatted select element
	 *
	 * @param string $name
	 * @param array $values
	 * @param string $selected element selected by default
	 * @param string $id
	 * @param string $class
	 * @return void
	 */
	public function select(string $name, array $values, string $selected = null, array $attr = null)
	{
		if (isset($this->_data[$name]))
			$selected = $this->_data[$name];

		$select = "<select name=\"$name\" ". Tag::attr($attr) .">";
		foreach ($values as $key => $value) {
			$is_select = ($key == $selected) ? 'selected' : '';
			$select .= "<option value=\"$key\" $is_select>$value</option>";
		}
		$select .= '</select>';

		return $select;
	}

	static public function s_select(string $name, array $values, $selected = null, string $attr = null)
	{
		$select = "<select name=\"$name\">";
		foreach ($values as $key => $value) {
			$is_select = ($key == $selected) ? 'selected' : '';
			$select .= "<option value=\"$key\" $is_select>$value</option>";
		}
		$select .= '</select>';

		return $select;
	}

	static public function gen_select(string $name, string $default_option, int $default_value, int $min_value, int $max_value, $default_selected)
	{
		$selected = ($default_selected == $default_value) ? 'selected' : '';
		$select = "<select name=\"$name\">";
		$select .= "<option value=\"$default_value\" $selected>$default_option</option>";
		for ($i = $min_value; $i <= $max_value; ++$i) {
			$selected = ($default_selected == $i) ? 'selected' : '';
			$digit = ($i < 10) ? "0" . $i : $i;
			$select .= "<option value=\"$i\" $selected>$digit</option>";
		}
		$select .= '</select>';
		return $select;
	}

	public function submit(string $content, array $attr = [] ): string
	{
		return Tag::tag('button', $content, $attr);
	}

	public function label(string $for, string $content, array $attr = []): string
	{
		$attr['for'] = $for;

		return Tag::tag('label', $content, $attr);
	}

	public function surround(string $name, string $label, string $input): string
	{
		return Tag::tag('p', $this->label($name, $label). '<br>' .$input);
	}

	public function linear_surround(string $name, string $label, string $input): string
	{
		return Tag::tag('p', $this->label($name, $label) . ' : ' . $input);
	}

	public function checkbox(string $name, $attr = null): string
	{
		if (isset($this->_data[$name]))
			$attr[] = 'checked';
	
		return $this->input($name, 'checkbox', $attr);
	}
}