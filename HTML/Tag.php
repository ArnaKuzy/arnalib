<?php

namespace Arnalib\HTML;

Class Tag
{
	/**
	 * Surround content with tag
	 *
	 * @param string $tag
	 * @param string $content
	 * @param array $attrs
	 * @return string return content surrounded
	 */
	public static function tag(string $tag, string $content, array $attrs = null): string
	{
		$attrs = self::attr($attrs);

		return "<$tag $attrs>$content</$tag>";
	}

	/**
	 * Get an array of html attribute and convert him to tag flag
	 * ex array('id' => 'link') = id="link"
	 *
	 * @param array $data an array of attributes
	 * @return string|null return string of attributes
	*/
	public static function attr(array $data = null): ?string
	{
		//var_dump($data);
		if (is_null($data))
			return null;
		$attrs = '';

		foreach ($data as $attr => $value) {
			if (is_string($attr) && !empty($value))
				$attrs .= " $attr=\"$value\" ";
			else
				$attrs .= " $value";
		}
		return $attrs;
	}

	/**
	 * generate html link
	 *
	 * @param string $link url
	 * @param string $content content
	 * @param array $attr
	 * @return string return html link
	 */
	public static function link(string $link, string $content, array $attr = []): string
	{
		$attr['href'] = $link;
		return self::tag('a', $content, $attr);
	}

	public static function img(string $src = ' ', string $alt = '', array $attr = [])
	{
		if (empty($alt))
			$alt = $src;

		return '<img src="'. $src .'" alt="'. $alt .'" '. self::attr($attr) .' />';
	}

	/**
	 * Generate html standard ul/li menu
	 *
	 * @param array $links
	 * @param array $attr
	 * @return string return generated menu
	 */
	public static function gen_ul_menu(array $links, array $attr = []): string
	{
		$menu = '';

		foreach ($links as $link) {
			if (!isset($link[2]))
				$link[2] = [];
			if (substr($link[0], 0, 1) != '/')
				$link[0] = '/' . $link[0];
			$menu .= self::tag('li', self::link($link[0], $link[1], $link[2]));
		}

		return self::tag('ul', $menu, $attr);
	}
}