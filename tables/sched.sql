CREATE TABLE IF NOT EXISTS `sched` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_schedule` int(11) NOT NULL,
  `day` tinyint(4) NOT NULL,
  `open_hour` tinyint(4) NOT NULL,
  `open_minute` tinyint(4) NOT NULL,
  `noon_hour` tinyint(4) NOT NULL,
  `noon_minute` tinyint(4) NOT NULL,
  `afternoon_hour` tinyint(4) NOT NULL,
  `afternoon_minute` tinyint(4) NOT NULL,
  `close_hour` tinyint(4) NOT NULL,
  `close_minute` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;