CREATE TABLE IF NOT EXISTS `schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `enable` tinyint(1) NOT NULL,
  `start_day` tinyint(4) NOT NULL,
  `start_month` tinyint(4) NOT NULL,
  `end_day` tinyint(4) NOT NULL,
  `end_month` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;