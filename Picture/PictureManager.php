<?php

namespace Arnalib\Picture;

use \Arnalib\CORE\Db;
use \Arnalib\HTML\Tag;
use \Arnalib\HTML\Form;
use \Arnalib\Picture\Picture;

class PictureManager
{
	use \Arnalib\CORE\GetInstance;

	/**
	 * Constants
	 */
    const PICTURE_CLASS			= 'Arnalib\Picture\Picture';
	/// ini_get('upload_max_filesize'); taille d'upload max du serveur

	private static $_instance;

	private $_db;
	private $_upload_path = 'img/upload/';

	public function	__construct()
	{
		$this->_db = Db::getInstance();
	}

	public function create_tables()
	{
		$this->_db->create_table('picture.sql');
	}

	public function upload_file($data): ?Picture
	{
		if ($data['error'] != 0)
			return null;

		$picture = new Picture($data);

		if(move_uploaded_file($data['tmp_name'], $this->_upload_path . $picture->file_key))
			return $picture;
		return null;
	}
	
	public function add($data, $file)
	{
		$picture = $this->upload_file($file);

		if (is_null($picture))
			return 'upload_fail';
		
		if (isset($data['name']) && !empty($data['name']))
			$picture->name = $data['name'];

		if ($this->_db->insert('picture', $picture->req_values()))
			return 'add_success';
		return 'upload_fail';
	}

	public function setter($id = null)
	{
		$name_for = 'name';
		$file_for = 'file';
		$button = 'Ajouter';
		$class = 'add';
		$preview = 'preview';
		$img = Tag::img('/img/loader.gif', '', [ 'id' => $preview ]);

		if (!is_null($id)) {
			$picture = $this->get($id);
			
			if (!$picture)
			return 'no_picture';
			
			$form = new Form($picture->data());
			$name_for = 'up_' . $name_for;
			$file_for = 'up_' . $file_for;
			$button = 'Modifier';
			$class = 'update';
			$preview = 'up_' . $preview;
			$img = Tag::img('/img/upload/' . $picture->file_key, $picture->name, [ 'id' => $preview ]);
		} else {
			$form = new Form();
		}

		$setter = "
				<p>
					<label for=\"$name_for\">Nom: </label>" .
					$form->input('name', 'text', [ 'id' => $name_for, 'placeholder' => 'tomate'] )
				. "</p>
				<p>
					<label for=\"$file_for\">Image :</label>".
					$form->input('file', 'file', [ 'id' => $file_for ]) .
					Tag::tag('button', $button, [ 'class' => $class, 'value' => $id ]).
					$img
				.'</p>';

		return $setter;
	}

	public function update(array $data, array $file, $id)
	{
		$picture = $this->get($id);

		if (isset($file['file'])) {
			$file = $file['file'];
		
			$new_picture = $this->upload_file($file);
			
			if (is_null($picture))
			return 'upload_fail';

			$picture->file_key = $new_picture->file_key;
		}
		
		if (isset($data['name']) && !empty($data['name']))
		$picture->name = $data['name'];

		if ($this->_db->update('picture', $picture->req_values(), $picture->data(), 'WHERE id = :id'))
			return 'update_success';
		return 'update_fail';
	}

	public function show_list()
	{
		foreach($this->list() as $picture) {
			$delete = ($picture->id == 0) ? '' : Tag::tag('button', 'Supprimer', [ 'class' => 'button delete-picture is-danger', 'value' => $picture->id ]);

			echo Tag::tag('div',
				Tag::tag('span', $picture->name) .
					Tag::img('/' . $this->_upload_path . $picture->file_key, $picture->name) .
					Tag::tag('button', 'Modifier', [ 'class' => ' button edit is-warning', 'value' => $picture->id ]) .
					$delete,
				[ 'id' => 'picture_' . $picture->id, 'class' => 'picture' ]);
		}
	}

	public function list(): ?array
	{
		if ($req = $this->_db->select('*', 'picture ORDER BY id')) {
			$req->setFetchMode(\PDO::FETCH_CLASS, self::PICTURE_CLASS);
			return $req->fetchAll();
		}

		return null;
	}

	public function array_list($usage): array
	{
		$pictures = array();

		if (!is_null($usage))
			switch($usage) {
				case "select":
					$pictures["0"] = 'Sélectionnez une image';
					break;
			}

		foreach($this->list() as $picture)
			$pictures[$picture->id] = $picture->name;

		return $pictures;
	}

	public function get($id): ?Picture
	{
		$req = $this->_db->select('*', 'picture', 'WHERE id = ?', array($id));
			
		if ($req) {
			$req->setFetchMode(\PDO::FETCH_CLASS, self::PICTURE_CLASS);
			$picture = $req->fetch();
			if ($picture)
				return $picture;
		}
		return null;
	}

	public function delete($id): string
	{
		$picture = $this->get($id);

		var_dump($this->_db->delete('picture WHERE id = ?', array( $id )));

		/*
		if (!$picture || !unlink($this->_upload_path . $picture->file_key))
			return 'delete_fail';
		*/

		return 'delete_success';
	}
} 
