<?php

namespace Arnalib\Picture;

use Arnalib\HTML\Tag;

class Picture
{
	use \Arnalib\CORE\Set;
	use \Arnalib\CORE\Get;

	const ALLOWED_EXTENSIONS 	= ['jpg', 'gif', 'png', 'jpeg' ];
    
	private	$_id;
	private $_name = 'default';
	private $_file_key = '0.png';

	public function __construct(array $data = null)
	{
		if (!is_null($data)) {
			$extension = pathinfo($data['name'], PATHINFO_EXTENSION);
			
			if (in_array(strtolower($extension), self::ALLOWED_EXTENSIONS)) {
				$this->name = $data['name'];
				$this->file_key = md5(uniqid()) .'.'. $extension;
			}
		}
	}
	
	public function show(array $attr = [])
	{
		return Tag::img($this->_file_key, $this->_name, $attr);
	}

	public function req_values()
	{
		return [ 'name' => $this->_name, 'file_key' => $this->_file_key ];
	}

	public function data()
	{
		return [ 'id' => $this->_id, 'name' => $this->_name, 'file_key' => $this->_file_key ];
	}
}