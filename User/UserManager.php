<?php

namespace Arnalib\User;

use Arnalib\CORE\Db;
use Arnalib\User;

class UserManager
{
	use CORE\GetInstance;
	private static $_instance;

	const USER_CLASS	= 'Arnalib\User\User';

	private $_db;

	public function __construct()
	{
		$this->_db = Db::getInstance();
	}

    /**
     * @param int $id
     */
    public function get_user($id)
	{
		$req = $this->_db->select('id, pseudo, email, creation_date, last_connection', 'users', 'WHERE id = ?', array($id));
		$req->setFetchMode(\PDO::FETCH_CLASS, self::USER_CLASS);
		return $req->fetch();
	}

    public function get_users($id)
	{
		$req = $this->_db->select('
			id				as _id,
			pseudo			as _pseudo,
			email			as _email,
			creation_date	as _creation_date,
			last_connection	as _last_connection', 'users', 'WHERE id <> ?', array($id));
		return $req->fetchall(\PDO::FETCH_CLASS, self::USER_CLASS);
	}

	public function user_exist(array $cols)
	{
		foreach($cols as $key => $val) {
			$req = $this->_db->select($key, 'users', "WHERE $key = ?", array($val))->fetch();
			if ($req)
				return true;
		}
		return false;
	}

	public function signin(array $post): string
	{
		if (!isset($post['pseudo']) ||
			!isset($post['password']) ||
			!isset($post['confirm_password']) ||
			!isset($post['email']) ||
			empty($post['pseudo']) ||
			empty($post['password']) ||
			empty($post['confirm_password']) ||
			empty($post['email'])
			)
			return 'form_not_full';

		if (!User::checkPseudo($post['pseudo']))
			return 'pseudo';
		if (!User::checkPassword($post['password']))
			return 'password';
		if ($post['password'] != $post['confirm_password'])
			return 'confirm_password';
		if (!User::checkEmail($post['email']))
			return 'email';
		if ($this->user_exist(array('pseudo' => $post['pseudo'], 'email' => $post['email'])))
			return 'user_exist';

		// Crypt password
		unset($post['confirm_password']);
		$post['password'] = password_hash($post['password'], PASSWORD_DEFAULT, ['cost' => 12]);
	
		// insert in db
		if (is_null($this->_db->insert('users', $post)))
			return 'fail';

		return 'signed';
	}

	public function keep_connect($user)
	{
		// Create cookie
		setcookie('7234', $user->id . '--' . hash('sha512', $user->password . $user->pseudo . $_SERVER['REMOTE_ADDR']),
			time() + 3600 * 24 * 3, '/', $_SERVER['SERVER_NAME'], false, true);
	}

	public function login_cookie($cookie)
	{
		// get ID
		$id = explode('--', $cookie);

		// get user from db
		$user = $this->get_user($id[0]);

		// if user exist compate key to user data
		if ($user) {
			$key = hash('sha512', $user->password . $user->pseudo . $_SERVER['REMOTE_ADDR']);
			// if exist make cookie
			if ($key == $id[1]) {
				$this->keep_connect($user);
				$_SESSION['user'] = $user;
			// else logoff
			} else
				$this->logoff();
		}
	}

	public function login(array $post)
	{
		// Check if user is already connected
		if (isset($_SESSION['user']))
			return 'already_connected';
		// Check if all inputs are full
		else if (!array_key_exists('pseudo', $post) || !array_key_exists('password', $post))
			return 'not_full_fill';
		
		// Check if user exist
		$req = $this->_db->select('id, pseudo, email', 'users', 'WHERE pseudo = ?',
				array( $post['pseudo'] ));
		$req->setFetchMode(\PDO::FETCH_CLASS, 'Arnalib\User');
		$user = $req->fetch();

		// If user don't exist stop login
		if (!$user)
			return 'bad_user-password';
		else if (!password_verify($post['password'], $user->password))
		// if keep_login checked make cookies
		if (isset($post['keep_connect']))
			$this->keep_connect($user);
		$_SESSION['user'] = $user;
		return 'connected';
	}

	public static function logoff()
	{
		setcookie('7234', '', time() - 1, '/', $_SERVER['SERVER_NAME'], false, true);
		unset($_SESSION['user']);
		session_destroy();
	}
}