<?php

namespace Arnalib\User;

class User
{
	use CORE\Get;
	use CORE\Set;

	private $_id;
	private $_pseudo;
	private $_password;
	private $_email;
	private $_creation_date;
	private $_last_connection;

	public function __construct(array $data = null)
	{
		if (!is_null($data)) {
			foreach ($data as $key => $value) {
				$this->$key = $value;
			}
		}
	}

	/*
	 * CHECKS
	 */

	public static function checkPseudo($value): bool
	{
		if (!is_string($value) || strlen($value) > 50)
			return false;
		return true;
	}

	public static function checkPassword($value): bool
	{
		if (!is_string($value))
			return false;
		return true;
	}

	public static function checkEmail($value): bool
	{
		if (!is_string($value) || !\filter_var($value, FILTER_VALIDATE_EMAIL))
			return false;
		return true;
	}

	/*
	 * GETTERS
	 */


	/*
	 * SETTERS
	 */

	public function setPseudo($value)
	{
		if (self::checkPseudo($value))
			$this->_pseudo = $value;
	}

	public function setPassword($value)
	{
		if (self::checkPassword($value))
			$this->_password = hash('sha512', $value);
	}

	public function setEmail($value)
	{
		if (self::checkEmail($value))
			$this->_email = $value;
	}
}