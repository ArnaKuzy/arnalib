<?php

namespace Arnalib\Schedule;

use \Arnalib\CORE\Db;
use \Arnalib\Schedule\Schedule;

class ScheduleManager
{
	use \Arnalib\CORE\GetInstance;

	const SCHEDULE_CLASS	= 'Arnalib\Schedule\Schedule';
	const SCHED_CLASS 		= 'Arnalib\Schedule\Sched';

	private static $_instance;
	
	private $_db;
	
	public function	__construct()
	{
		$this->_db = Db::getInstance();
	}

	public function create_tables()
	{
		$this->_db->create_table('schedule.sql');
		$this->_db->create_table('sched.sql');
	}
	
	public function add()
	{
		$schedule = new Schedule();
		
		$this->_db->insert('schedule',
			array(	'name' 			=> $schedule->name,
					'enable'		=> $schedule->enable,
					'start_day'		=> $schedule->start_day,
					'start_month'	=> $schedule->start_month,
					'end_day'		=> $schedule->end_day,
					'end_month'		=> $schedule->end_month)
		);
		$schedule->id = $this->_db->last_insert_id();
		$schedule->init($schedule->id);

		// Insert day
		$days = array();
		foreach($schedule->sched as $sched) {
			$days[] = array(
				'id_schedule'		=> $sched->id_schedule,
				'day'				=> $sched->day,
				'open_hour'			=> $sched->open_hour,
				'open_minute'		=> $sched->open_minute,
				'noon_hour'			=> $sched->noon_hour,
				'noon_minute'		=> $sched->noon_minute,
				'afternoon_hour'	=> $sched->afternoon_hour,
				'afternoon_minute'	=> $sched->afternoon_minute,
				'close_hour'		=> $sched->close_hour,
				'close_minute'		=> $sched->close_minute);
		}
		$this->_db->multi_insert('sched', $days);

		return $this->last();
	}

	public function update(array $data)
	{
		$data['enable'] = ($data['enable'] == 'true') ? 1 : 0;
		$schedule = new Schedule($data);

		$sets = $schedule->data();
		unset($sets['id']);
		$this->_db->update('schedule', $sets, $schedule->data(), 'WHERE id = :id');

		foreach($schedule->sched as $day) {
			$this->_db->update('sched', $day->sets(), $day->data(), 'WHERE id_schedule = :id_schedule AND day = :day');
		}

		return $this->get_class($schedule->id)->show2();
	}

	public function list()
	{
		$req = $this->_db->select('*', 'schedule ORDER BY id');
		
		if ($req) {
			$req->setFetchMode(\PDO::FETCH_CLASS, self::SCHEDULE_CLASS);
			$list = $req->fetchAll();
			$schedules = "";
			foreach ($list as $schedule) {
				for ($day = 0; $day < 7; ++$day) {
					$req = $this->_db->select('*', 'sched', 'WHERE id_schedule = ? AND day = ?', array( $schedule->id, $day ));
					if ($req) {
						$req->setFetchMode(\PDO::FETCH_CLASS, self::SCHED_CLASS);
						$sched = $req->fetch();
						if ($sched)
							$schedule->add_sched($sched, $day);
					} else {
						$schedule->add_sched(null, $day);
					}
				}
				$schedules .= $schedule->schedule_setter();
			}
			return $schedules;
		} else {
			return "Pas d'horaires !";
		}
	}

	public function get($id)
	{
		$req = $this->_db->select('*', 'schedule', 'WHERE id = ?', array($id));
		
		if ($req) {
			$req->setFetchMode(\PDO::FETCH_CLASS, self::SCHEDULE_CLASS);
			$schedule = $req->fetch();
			
			for ($day = 0; $day < 7; ++$day) {
				$req = $this->_db->select('*', 'sched', 'WHERE id_schedule = ? AND day = ?', array( $schedule->id, $day ));
				if ($req) {
					$req->setFetchMode(\PDO::FETCH_CLASS, self::SCHED_CLASS);
					$sched = $req->fetch();
					if ($sched)
						$schedule->add_sched($sched, $day);
				} else {
					$schedule->add_sched(null, $day);
				}
			}
			return $schedule->schedule_setter();
		}
	}

	public function get_class($id)
	{
		$req = $this->_db->select('*', 'schedule', 'WHERE id = ?', array($id));
		
		if ($req) {
			$req->setFetchMode(\PDO::FETCH_CLASS, self::SCHEDULE_CLASS);
			$schedule = $req->fetch();
			
			for ($day = 0; $day < 7; ++$day) {
				$req = $this->_db->select('*', 'sched', 'WHERE id_schedule = ? AND day = ?', array( $schedule->id, $day ));
				if ($req) {
					$req->setFetchMode(\PDO::FETCH_CLASS, self::SCHED_CLASS);
					$sched = $req->fetch();
					if ($sched)
						$schedule->add_sched($sched, $day);
				} else {
					$schedule->add_sched(null, $day);
				}
			}
			return $schedule;
		}
		return null;
	}

	public function last()
	{
		$req = $this->_db->select('*', 'schedule', 'WHERE id = (SELECT MAX(id) FROM schedule)');
		
		if ($req) {
			$req->setFetchMode(\PDO::FETCH_CLASS, self::SCHEDULE_CLASS);
			$schedule = $req->fetch();
			
			for ($day = 0; $day < 7; ++$day) {
				$req = $this->_db->select('*', 'sched', 'WHERE id_schedule = ? AND day = ?', array( $schedule->id, $day ));
				if ($req) {
					$req->setFetchMode(\PDO::FETCH_CLASS, self::SCHED_CLASS);
					$sched = $req->fetch();
					if ($sched)
						$schedule->add_sched($sched, $day);
				} else {
					$schedule->add_sched(null, $day);
				}
			}
			return $schedule->schedule_setter();
		}
	}

	public function get_now()
	{
		$req = $this->_db->select('*', 'schedule',
			'WHERE	(start_month = :month AND start_day <= :day) OR
					(start_month < :month AND end_month > :month) OR
					(end_day >= :day AND end_month = :month)',
			[ 'day' => date('d'), 'month' => date('m') ]);
		
		if ($req) {
			$req->setFetchMode(\PDO::FETCH_CLASS, self::SCHEDULE_CLASS);
			$schedule = $req->fetch();

			if (!$schedule || !$schedule->enable)
				return null;
				
			for ($day = 0; $day < 7; ++$day) {
				$req = $this->_db->select('*', 'sched', 'WHERE id_schedule = ? AND day = ?', array( $schedule->id, $day ));
				if ($req) {
					$req->setFetchMode(\PDO::FETCH_CLASS, self::SCHED_CLASS);
					$sched = $req->fetch();
					if ($sched)
						$schedule->add_sched($sched, $day);
				} else {
					$schedule->add_sched(null, $day);
				}
			}
			return $schedule;
		} else
			return null;
	}

	public function delete($id)
	{
		if ($this->_db->delete("schedule WHERE id = ?", array($id)) &&
			$this->_db->delete("sched WHERE id_schedule = ?", array($id)))
			return 'delete_success';
		return 'delete_fail';
	}
}