<?php

namespace Arnalib\Schedule;

use \Arnalib\HTML\Form;
use \Arnalib\HTML\Table;
use \Arnalib\HTML\Tag;
use \Arnalib\Schedule\Sched;

class Schedule
{
	use \Arnalib\CORE\Get;
	use \Arnalib\CORE\Set;

	const FRENCH_MONTH = array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre');
	const FRENCH_DAY	= array('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche');

	private $_id = 0;
	private $_name = "Nouveau";
	private $_enable = 1;
	private $_start_day = 1;
	private $_start_month = 0;
	private $_end_day = 31;
	private $_end_month = 11;
	private $_sched = array();
	
	private $_default;
	private $_holiday;
	private $_holiday_date = array();

	public function __construct(array $data = array())
	{
		if (!empty($data)) {
			foreach ($data as $key => $value) {
				$key2 = '_' . $key;
				if (isset($this->$key2)) {
					$this->$key2 = $value;
					unset($data[$key]);
				}
			}
		}
		if (!empty($data)) {
			$data = array_chunk($data, 8, true);
			$i = 0;
			foreach($data as $day) {
				$keys = array_keys($day);
				$day['day'] = substr(array_shift($keys), 0, 1);
				$day['id_schedule'] = $this->id;
				$this->add_sched(new Sched($day), $i++);
			}
		}
	}

	public function data(): array
	{
		return array(
			'id'			=> $this->id,
			'name'			=> $this->name,
			'enable'		=> $this->enable,
			'start_day'		=> $this->start_day,
			'start_month'	=> $this->start_month,
			'end_day'		=> $this->end_day,
			'end_month'		=> $this->end_month,
		);
	}

	public function setHoliday(array $default)
	{
		$this->_holiday = $default;
	}

	public function setDefault(array $default)
	{
		$this->_default = $default;
	}

	public function addHoliday(array $months)
	{
		foreach ($months as $value)
			if ($value >= 1 && $value <= 12)
				$this->_holiday_date[] = $value;
	}

	public function show(array $attr = []): string
	{
		// Get schedule related to date
		$schedule = (!is_null($this->_holiday) && in_array(date('m' ), $this->_holiday_date)) ? $this->_holiday : $this->_default;

		// Convert on schedule table
		$table = '';

		// Header 
		$table .= Table::header(array('', 'Matin', 'Soir'));

		// Content
		for ($i = 0; $i < 7; ++$i)
			$table .= '<tr><th>' . self::FRENCH_DAY[$i] . '</th>'.
				Table::col($schedule[$i][0]) .
				Table::col($schedule[$i][1]) . '</tr>';

		$table = Tag::tag('table', $table, $attr);
		return $table;
	}

	public function show2(array $attr = []): string
	{
		$schedule = array( [ '', 'Matin', 'Après-midi' ] );

		foreach ($this->sched as $day) {
			$schedule[] = array(
				$day::FRENCH_DAY[$day->day],
				$day->getMorning(),
				$day->getAfternoon()
			);
		}

		return Tag::tag('div', Table::makeTable($schedule), $attr);
	}

	public function init(int $id_schedule)
	{
		for ($i = 0; $i < 7; ++$i)
			$this->add_sched(
				new Sched(
					array(
						'id_schedule' => $id_schedule,
						'day' => $i)
					),
				$i
				);
	}

	public function add_sched(Sched $sched, int $day)
	{
		if ($day < 7)
			$this->_sched[$day] = $sched;
	}

	public function schedule_setter()
	{
		$form = new Form();

		$enable = ($this->enable) ? 'checked' : '';

		$sched_header = array(
			[ 
				$form->input('name', 'text', array('value' => $this->name, 'placeholder' => 'vacances')),
				'' ,
				Tag::tag('button', 'Sauvegarger', [ 'class' => 'save success', 'value' => $this->id ]),
				Tag::tag('button', 'Supprimer', [ 'class' => 'delete fail', 'value' => $this->id ])
			],
			[
				'Du ' .
					Form::gen_select('start_day', 1, 1, 2, 31, $this->start_day) .
					Form::s_select("start_month", self::FRENCH_MONTH, $this->start_month),
				'Au ' . Form::gen_select("end_day", 1, 1, 2, 31, $this->end_day) .
					Form::s_select("end_month", self::FRENCH_MONTH, $this->end_month),
				'',
				'Activer' .	$form->input("enable", "checkbox", array( $enable )),
			]
		);

		$sched_header = Table::makeTable($sched_header);
				
		$sched_body = array(
			[
				'',
				'Matin',
				'Après-midi'
			]
		);

		foreach ($this->sched as $sched)
			$sched_body[] = $sched->form();
		
		$sched_body = Table::makeTable($sched_body);

		$sched_setter = Tag::tag('div', $sched_header . $sched_body, [ 'id' => 'setter_' . $this->id, 'class' => 'center' ]);

		return Tag::tag(
			'div',
			$sched_setter . $this->show2( [ 'id' => 'result_' . $this->id, 'class' => 'flex center' ]),
			array(
				'id' 	=> "schedule_$this->id",
				'class'	=> 'schedule flex container'
			));
	}
}