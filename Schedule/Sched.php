<?php

namespace Arnalib\Schedule;

use \Arnalib\HTML\Form;
use \Arnalib\HTML\Tag;

class Sched
{
	use \Arnalib\CORE\Set;
	use \Arnalib\CORE\Get;

	const FRENCH_DAY	= array('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche');
	const SELECT_NAME	= array('open', 'noon', 'afternoon', 'close');
	private	$_id = 0;
	private	$_id_schedule = 0;
	private	$_day = 0;
	private	$_open_hour = -1;
	private	$_open_minute = -1;
	private	$_noon_hour = -1;
	private	$_noon_minute = -1;
	private	$_afternoon_hour = -1;
	private	$_afternoon_minute = -1;
	private	$_close_hour = -1;
	private	$_close_minute = -1;

	public function __construct(array $data = null)
	{
		if (!is_null($data)) {
			foreach ($data as $key => $value) {
				if (is_numeric(substr($key, 0, 1))) {
					$key = substr($key, 1);
					$this->$key = $value;
				} else {
					$key = '_' . $key;
					$this->$key = $value;
				}
			}
		}
	}

	public function sets(): array
	{
		return array(
			'open_hour'			=> $this->open_hour,
			'open_minute'		=> $this->open_minute,
			'noon_hour'			=> $this->noon_hour,
			'noon_minute'		=> $this->noon_minute,
			'afternoon_hour'	=> $this->afternoon_hour,
			'afternoon_minute'	=> $this->afternoon_minute,
			'close_hour'		=> $this->close_hour,
			'close_minute'		=> $this->close_minute
		);
	}

	public function data(): array
	{
		return array(
			'id_schedule'		=> $this->id_schedule,
			'day'				=> $this->day,
			'open_hour'			=> $this->open_hour,
			'open_minute'		=> $this->open_minute,
			'noon_hour'			=> $this->noon_hour,
			'noon_minute'		=> $this->noon_minute,
			'afternoon_hour'	=> $this->afternoon_hour,
			'afternoon_minute'	=> $this->afternoon_minute,
			'close_hour'		=> $this->close_hour,
			'close_minute'		=> $this->close_minute
		);
	}

	private function time_setter($col_id): string
	{
		$prefix = $this->day . '_' . self::SELECT_NAME[$col_id];
		$default_hour = self::SELECT_NAME[$col_id] . '_hour';
		$default_minute = self::SELECT_NAME[$col_id] . '_minute';
		return Form::gen_select( $prefix . '_hour', '--', -1, 0, 12, $this->$default_hour) .
			" H " .
			Form::gen_select( $prefix . '_minute', '--', -1, 0, 59, $this->$default_minute);
	}

	public function form(): array
	{
		$form = array ( self::FRENCH_DAY[$this->day] );

		$form[] = $this->time_setter(0) . ' - ' . $this->time_setter(1);
		$form[] = $this->time_setter(2) . ' - ' . $this->time_setter(3);

		return $form;
	}

	private function time(string $hour, string $minute)
	{
		$hour = ($hour < 10) ? "0$hour" : $hour;
		$minute = ($minute < 10) ? "0$minute" : $minute;

		return $hour . 'H' .$minute;

	}

	/* GETTERS */
	/**
	 * Return formated morning schedule
	 *
	 * @return string formated schedule
	 */
	public function getMorning(): string
	{
		if ($this->open_hour == -1 || $this->open_minute == -1)
			return 'Fermé';

		$open = self::time($this->open_hour, $this->open_minute);
			
		$noon = ($this->noon_hour != -1 && $this->_noon_minute != -1) ?
			' - ' . self::time($this->noon_hour, $this->noon_minute) : '';

		return $open . $noon;
	}

	/**
	 * Return formated afternoon schedule
	 *
	 * @return string formated schedule
	 */
	public function getAfternoon(): string
	{
		if ($this->close_hour == -1 || $this->close_minute == -1)
			return 'Fermé';

		$close = self::time($this->close_hour, $this->close_minute);
			
		$afternoon = ($this->afternoon_hour != -1 && $this->_afternoon_minute != -1) ?
			self::time($this->afternoon_hour, $this->afternoon_minute) . ' - ' : '';

		return $afternoon . $close;
	}
}